package com.fikiap.chatbrobackendapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class ChatbroBackendApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChatbroBackendApiApplication.class, args);
	}

}
