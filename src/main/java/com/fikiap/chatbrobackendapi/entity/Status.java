package com.fikiap.chatbrobackendapi.entity;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Kelas entitas untuk menyimpan informasi status.
 */
public class Status {
    private String uid;
    private String username;
    private String phoneNumber;
    private List<String> photoUrl;
    private Date createdAt;
    private String profilePic;
    private List<String> captions;
    private String statusId;
    private List<String> whoCanSee;

    /**
     * Mengembalikan ID pengguna yang memiliki status.
     *
     * @return ID pengguna
     */
    public String getUid() {
        return uid;
    }

    /**
     * Mengatur ID pengguna yang memiliki status.
     *
     * @param uid ID pengguna yang akan diatur
     */
    public void setUid(String uid) {
        this.uid = uid;
    }

    /**
     * Mengembalikan username pengguna yang memiliki status.
     *
     * @return Username pengguna
     */
    public String getUsername() {
        return username;
    }

    /**
     * Mengatur username pengguna yang memiliki status.
     *
     * @param username Username pengguna yang akan diatur
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Mengembalikan nomor telepon pengguna yang memiliki status.
     *
     * @return Nomor telepon pengguna
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Mengatur nomor telepon pengguna yang memiliki status.
     *
     * @param phoneNumber Nomor telepon pengguna yang akan diatur
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Mengembalikan daftar URL foto pada status.
     *
     * @return Daftar URL foto
     */
    public List<String> getPhotoUrl() {
        return photoUrl;
    }

    /**
     * Mengatur daftar URL foto pada status.
     *
     * @param photoUrl Daftar URL foto yang akan diatur
     */
    public void setPhotoUrl(List<String> photoUrl) {
        this.photoUrl = photoUrl;
    }

    /**
     * Mengembalikan tanggal dan waktu pembuatan status.
     *
     * @return Tanggal dan waktu pembuatan status
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * Mengatur tanggal dan waktu pembuatan status.
     *
     * @param createdAt Tanggal dan waktu pembuatan status yang akan diatur
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Mengembalikan URL foto profil pengguna yang memiliki status.
     *
     * @return URL foto profil pengguna
     */
    public String getProfilePic() {
        return profilePic;
    }

    /**
     * Mengatur URL foto profil pengguna yang memiliki status.
     *
     * @param profilePic URL foto profil pengguna yang akan diatur
     */
    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    /**
     * Mengembalikan daftar caption pada status.
     *
     * @return Daftar caption
     */
    public List<String> getCaptions() {
        return captions;
    }

    /**
     * Mengatur daftar caption pada status.
     *
     * @param captions Daftarcaption yang akan diatur
     */
    public void setCaptions(List<String> captions) {
        this.captions = captions;
    }

    /**
     * Mengembalikan ID status.
     *
     * @return ID status
     */
    public String getStatusId() {
        return statusId;
    }

    /**
     * Mengatur ID status.
     *
     * @param statusId ID status yang akan diatur
     */
    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    /**
     * Mengembalikan daftar ID pengguna yang dapat melihat status.
     *
     * @return Daftar ID pengguna
     */
    public List<String> getWhoCanSee() {
        return whoCanSee;
    }

    /**
     * Mengatur daftar ID pengguna yang dapat melihat status.
     *
     * @param whoCanSee Daftar ID pengguna yang akan diatur
     */
    public void setWhoCanSee(List<String> whoCanSee) {
        this.whoCanSee = whoCanSee;
    }

    /**
     * Mengembalikan Map yang berisi data dari objek Status.
     *
     * @return Map yang berisi data dari objek Status
     */
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("uid", uid);
        map.put("username", username);
        map.put("phoneNumber", phoneNumber);
        map.put("photoUrl", photoUrl);
        map.put("createdAt", createdAt);
        map.put("profilePic", profilePic);
        map.put("captions", captions);
        map.put("statusId", statusId);
        map.put("whoCanSee", whoCanSee);

        return map;
    }
}
