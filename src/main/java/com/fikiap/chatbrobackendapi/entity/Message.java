package com.fikiap.chatbrobackendapi.entity;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Kelas entitas untuk menyimpan informasi pesan.
 */
public class Message {
    private String senderId;
    private String receiverId;
    private String text;
    private String type;
    private Date timeSent;
    private String messageId;
    private boolean isSeen;
    private String repliedMessage;
    private String repliedTo;
    private String repliedMessageType;

    /**
     * Mengembalikan ID pengirim pesan.
     *
     * @return ID pengirim pesan
     */
    public String getSenderId() {
        return senderId;
    }

    /**
     * Mengatur ID pengirim pesan.
     *
     * @param senderId ID pengirim pesan yang akan diatur
     */
    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    /**
     * Mengembalikan ID penerima pesan.
     *
     * @return ID penerima pesan
     */
    public String getReceiverId() {
        return receiverId;
    }

    /**
     * Mengatur ID penerima pesan.
     *
     * @param receiverId ID penerima pesan yang akan diatur
     */
    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    /**
     * Mengembalikan teks pesan.
     *
     * @return Teks pesan
     */
    public String getText() {
        return text;
    }

    /**
     * Mengatur teks pesan.
     *
     * @param text Teks pesan yang akan diatur
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Mengembalikan tipe pesan.
     *
     * @return Tipe pesan
     */
    public String getType() {
        return type;
    }

    /**
     * Mengatur tipe pesan.
     *
     * @param type Tipe pesan yang akan diatur
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Mengembalikan waktu pengiriman pesan.
     *
     * @return Waktu pengiriman pesan
     */
    public Date getTimeSent() {
        return timeSent;
    }

    /**
     * Mengatur waktu pengiriman pesan.
     *
     * @param timeSent Waktu pengiriman pesan yang akan diatur
     */
    public void setTimeSent(Date timeSent) {
        this.timeSent = timeSent;
    }

    /**
     * Mengembalikan ID pesan.
     *
     * @return ID pesan
     */
    public String getMessageId() {
        return messageId;
    }

    /**
     * Mengatur ID pesan.
     *
     * @param messageId ID pesan yang akan diatur
     */
    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    /**
     * Mengembalikan status baca pesan.
     *
     * @return Status baca pesan
     */
    public boolean isSeen() {
        return isSeen;
    }

    /**
     * Mengatur status baca pesan.
     *
     * @param seen Status baca pesan yang akan diatur
     */
    public void setSeen(boolean seen) {
        isSeen = seen;
    }

    /**
     * Mengembalikan pesanyang dijawab oleh pesan ini.
     *
     * @return Pesan yang dijawab
     */
    public String getRepliedMessage() {
        return repliedMessage;
    }

    /**
     * Mengatur pesan yang dijawab oleh pesan ini.
     *
     * @param repliedMessage Pesan yang dijawab
     */
    public void setRepliedMessage(String repliedMessage) {
        this.repliedMessage = repliedMessage;
    }

    /**
     * Mengembalikan ID pesan yang dijawab oleh pesan ini.
     *
     * @return ID pesan yang dijawab
     */
    public String getRepliedTo() {
        return repliedTo;
    }

    /**
     * Mengatur ID pesan yang dijawab oleh pesan ini.
     *
     * @param repliedTo ID pesan yang dijawab
     */
    public void setRepliedTo(String repliedTo) {
        this.repliedTo = repliedTo;
    }

    /**
     * Mengembalikan tipe pesan yang dijawab oleh pesan ini.
     *
     * @return Tipe pesan yang dijawab
     */
    public String getRepliedMessageType() {
        return repliedMessageType;
    }

    /**
     * Mengatur tipe pesan yang dijawab oleh pesan ini.
     *
     * @param repliedMessageType Tipe pesan yang dijawab
     */
    public void setRepliedMessageType(String repliedMessageType) {
        this.repliedMessageType = repliedMessageType;
    }

    /**
     * Mengembalikan Map yang berisi data dari objek Message.
     *
     * @return Map yang berisi data dari objek Message
     */
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("senderId", senderId);
        map.put("receiverId", receiverId);
        map.put("text", text);
        map.put("type", type);
        map.put("timeSent", timeSent);
        map.put("messageId", messageId);
        map.put("isSeen", isSeen);
        map.put("repliedMessage", repliedMessage);
        map.put("repliedTo", repliedTo);
        map.put("repliedMessageType", repliedMessageType);

        return map;
    }
}
