package com.fikiap.chatbrobackendapi.entity;

import java.util.HashMap;
import java.util.Map;

/**
 * Representasi objek Admin.
 */
public class Admin {
    private String username;
    private String uid;
    private String profilePic;
    private String email;

    /**
     * Mengembalikan username admin.
     *
     * @return Username admin.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Mengatur username admin.
     *
     * @param username Username admin.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Mengembalikan UID admin.
     *
     * @return UID admin.
     */
    public String getUid() {
        return uid;
    }

    /**
     * Mengatur UID admin.
     *
     * @param uid UID admin.
     */
    public void setUid(String uid) {
        this.uid = uid;
    }

    /**
     * Mengembalikan URL foto profil admin.
     *
     * @return URL foto profil admin.
     */
    public String getProfilePic() {
        return profilePic;
    }

    /**
     * Mengatur URL foto profil admin.
     *
     * @param profilePic URL foto profil admin.
     */
    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    /**
     * Mengembalikan email admin.
     *
     * @return Email admin.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Mengatur email admin.
     *
     * @param email Email admin.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Mengubah objek Admin menjadi map.
     *
     * @return Map yang berisi data Admin.
     */
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("username", username);
        map.put("uid", uid);
        map.put("profilePic", profilePic);
        map.put("email", email);
        return map;
    }
}
