package com.fikiap.chatbrobackendapi.entity;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Kelas entitas untuk menyimpan informasi kontak chat.
 */
public class ChatContact {
    private String name;
    private String profilePic;
    private String contactId;
    private Date timeSent;
    private String lastMessage;
    private String phoneNumber;

    /**
     * Mengembalikan nama kontak.
     *
     * @return Nama kontak
     */
    public String getName() {
        return name;
    }

    /**
     * Mengatur nama kontak.
     *
     * @param name Nama kontak yang akan diatur
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Mengembalikan URL gambar profil kontak.
     *
     * @return URL gambar profil kontak
     */
    public String getProfilePic() {
        return profilePic;
    }

    /**
     * Mengatur URL gambar profil kontak.
     *
     * @param profilePic URL gambar profil kontak yang akan diatur
     */
    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    /**
     * Mengembalikan ID kontak.
     *
     * @return ID kontak
     */
    public String getContactId() {
        return contactId;
    }

    /**
     * Mengatur ID kontak.
     *
     * @param contactId ID kontak yang akan diatur
     */
    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    /**
     * Mengembalikan waktu pengiriman terakhir pesan.
     *
     * @return Waktu pengiriman terakhir pesan
     */
    public Date getTimeSent() {
        return timeSent;
    }

    /**
     * Mengatur waktu pengiriman terakhir pesan.
     *
     * @param timeSent Waktu pengiriman terakhir pesan yang akan diatur
     */
    public void setTimeSent(Date timeSent) {
        this.timeSent = timeSent;
    }

    /**
     * Mengembalikan pesan terakhir dari kontak.
     *
     * @return Pesan terakhir dari kontak
     */
    public String getLastMessage() {
        return lastMessage;
    }

    /**
     * Mengatur pesan terakhir dari kontak.
     *
     * @param lastMessage Pesan terakhir dari kontak yang akan diatur
     */
    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    /**
     * Mengembalikan nomor telepon kontak.
     *
     * @return Nomor telepon kontak
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Mengatur nomor telepon kontak.
     *
     * @param phoneNumber Nomor telepon kontak yang akan diatur
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Mengonversi objek ChatContact menjadi Map yang dapat digunakan untuk menyimpan data ke database.
     *
     * @return Map yang berisi data ChatContact
     */
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("name", name);
        map.put("profilePic", profilePic);
        map.put("contactId", contactId);
        map.put("timeSent", timeSent);
        map.put("lastMessage", lastMessage);
        map.put("phoneNumber", phoneNumber);
        return map;
    }
}
