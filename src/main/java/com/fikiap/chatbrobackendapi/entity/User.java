package com.fikiap.chatbrobackendapi.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Entitas yang merepresentasikan pengguna.
 */
public class User {
    private String name;
    private String uid;
    private String profilePic;
    private boolean isOnline;
    private String phoneNumber;
    private List<String> groupId;

    /**
     * Mengembalikan nama pengguna.
     *
     * @return Nama pengguna
     */
    public String getName() {
        return name;
    }

    /**
     * Mengatur nama pengguna.
     *
     * @param name Nama pengguna
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Mengembalikan ID pengguna.
     *
     * @return ID pengguna
     */
    public String getUid() {
        return uid;
    }

    /**
     * Mengatur ID pengguna.
     *
     * @param uid ID pengguna
     */
    public void setUid(String uid) {
        this.uid = uid;
    }

    /**
     * Mengembalikan URL gambar profil pengguna.
     *
     * @return URL gambar profil pengguna
     */
    public String getProfilePic() {
        return profilePic;
    }

    /**
     * Mengatur URL gambar profil pengguna.
     *
     * @param profilePic URL gambar profil pengguna
     */
    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    /**
     * Memeriksa status pengguna apakah online atau tidak.
     *
     * @return true jika pengguna online, false jika tidak
     */
    public boolean isOnline() {
        return isOnline;
    }

    /**
     * Mengatur status pengguna apakah online atau tidak.
     *
     * @param isOnline true jika pengguna online, false jika tidak
     */
    public void setOnline(boolean isOnline) {
        this.isOnline = isOnline;
    }

    /**
     * Mengembalikan nomor telepon pengguna.
     *
     * @return Nomor telepon pengguna
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Mengatur nomor telepon pengguna.
     *
     * @param phoneNumber Nomor telepon pengguna
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Mengembalikan daftar ID grup yang dimiliki pengguna.
     *
     * @return Daftar ID grup pengguna
     */
    public List<String> getGroupId() {
        return groupId;
    }

    /**
     * Mengatur daftar ID grup yang dimiliki pengguna.
     *
     * @param groupId Daftar ID grup pengguna
     */
    public void setGroupId(List<String> groupId) {
        this.groupId = groupId;
    }

    /**
     * Mengkonversi objek User menjadi Map yang berisi atribut-atribut pengguna.
     *
     * @return Map yang berisi atribut-atribut pengguna
     */
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("name", name);
        map.put("uid", uid);
        map.put("profilePic", profilePic);
        map.put("isOnline", isOnline);
        map.put("phoneNumber", phoneNumber);
        map.put("groupId", groupId);
        return map;
    }
}
