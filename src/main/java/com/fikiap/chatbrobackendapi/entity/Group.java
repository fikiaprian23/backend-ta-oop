package com.fikiap.chatbrobackendapi.entity;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Kelas entitas untuk menyimpan informasi grup.
 */
public class Group {
    private String senderId;
    private String name;
    private String groupId;
    private String lastMessage;
    private String groupPic;
    private List<String> membersUid;
    private Date timeSent;

    /**
     * Mengatur ID pengirim grup.
     *
     * @param senderId ID pengirim grup yang akan diatur
     */
    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    /**
     * Mengatur nama grup.
     *
     * @param name Nama grup yang akan diatur
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Mengatur ID grup.
     *
     * @param groupId ID grup yang akan diatur
     */
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    /**
     * Mengatur pesan terakhir grup.
     *
     * @param lastMessage Pesan terakhir grup yang akan diatur
     */
    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    /**
     * Mengatur URL gambar grup.
     *
     * @param groupPic URL gambar grup yang akan diatur
     */
    public void setGroupPic(String groupPic) {
        this.groupPic = groupPic;
    }

    /**
     * Mengatur daftar ID anggota grup.
     *
     * @param membersUid Daftar ID anggota grup yang akan diatur
     */
    public void setMembersUid(List<String> membersUid) {
        this.membersUid = membersUid;
    }

    /**
     * Mengatur waktu pengiriman grup.
     *
     * @param timeSent Waktu pengiriman grup yang akan diatur
     */
    public void setTimeSent(Date timeSent) {
        this.timeSent = timeSent;
    }

    /**
     * Mengembalikan ID pengirim grup.
     *
     * @return ID pengirim grup
     */
    public String getSenderId() {
        return senderId;
    }

    /**
     * Mengembalikan nama grup.
     *
     * @return Nama grup
     */
    public String getName() {
        return name;
    }

    /**
     * Mengembalikan ID grup.
     *
     * @return ID grup
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Mengembalikan pesan terakhir grup.
     *
     * @return Pesan terakhir grup
     */
    public String getLastMessage() {
        return lastMessage;
    }

    /**
     * Mengembalikan URL gambar grup.
     *
     * @return URL gambar grup
     */
    public String getGroupPic() {
        return groupPic;
    }

    /**
     * Mengembalikan daftar ID anggota grup.
     *
     * @return Daftar ID anggota grup
     */
    public List<String> getMembersUid() {
        return membersUid;
    }

    /**
     * Mengembalikan waktu pengiriman grup.
     *
     * @return Waktu pengiriman grup
     */
    public Date getTimeSent() {
        return timeSent;
    }

    /**
     * Mengonversi objek Group menjadi Map yang dapat digunakanuntuk menyimpan data ke Firebase Firestore.

     * @return Map yang berisi data dari objek Group
     */
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("senderId", senderId);
        map.put("name", name);
        map.put("groupId", groupId);
        map.put("lastMessage", lastMessage);
        map.put("groupPic", groupPic);
        map.put("membersUid", membersUid);
        map.put("timeSent", timeSent.getTime());
        return map;
    }
}
