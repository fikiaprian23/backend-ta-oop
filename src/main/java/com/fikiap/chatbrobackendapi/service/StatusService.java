package com.fikiap.chatbrobackendapi.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fikiap.chatbrobackendapi.config.DatabaseConnection;
import com.fikiap.chatbrobackendapi.entity.Status;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.FieldValue;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;

/**
 * Kelas Service yang menangani operasi terkait status.
 */
@Service
public class StatusService {
    private static final String COLLECTION_NAME = "status";
    private final Firestore dbFirestore;

    public StatusService(DatabaseConnection databaseConnection) {
        this.dbFirestore = databaseConnection.getFirestore();
    }

    /**
     * Menyimpan status ke database.
     *
     * @param status Status yang akan disimpan
     * @return ResponseEntity dengan pesan sukses atau respons kesalahan
     */
    public ResponseEntity<Map<String, Object>> saveStatus(Status status) {
        try {
            dbFirestore.collection(COLLECTION_NAME)
                    .document(status.getUid())
                    .set(status);

            Map<String, Object> response = new HashMap<>();
            response.put("code", "200");
            response.put("status", "OK");
            response.put("data", status);

            return ResponseEntity.ok(response);
        } catch (Exception e) {
            Map<String, Object> response = new HashMap<>();
            response.put("code", "500");
            response.put("status", "Internal Server Error");
            response.put("errors", e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    /**
     * Mengambil detail status berdasarkan UID.
     *
     * @param uid UID status
     * @return ResponseEntity dengan detail status atau respons not found
     */
    public ResponseEntity<Map<String, Object>> getStatusDetails(String uid) {
        try {
            DocumentReference documentReference = dbFirestore.collection(COLLECTION_NAME).document(uid);
            ApiFuture<DocumentSnapshot> future = documentReference.get();
            DocumentSnapshot documentSnapshot = future.get();

            if (documentSnapshot.exists()) {
                Status status = documentSnapshot.toObject(Status.class);

                Map<String, Object> response = new HashMap<>();
                response.put("code", "200");
                response.put("status", "OK");
                response.put("data", status);

                return ResponseEntity.ok(response);
            } else {
                Map<String, Object> response = new HashMap<>();
                response.put("code", "404");
                response.put("status", "Not Found");

                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
            }
        } catch (Exception e) {
            Map<String, Object> response = new HashMap<>();
            response.put("code", "500");
            response.put("status", "Internal Server Error");
            response.put("errors", e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    /**
     * Memperbarui status.
     *
     * @param status Status yang diperbarui
     * @return ResponseEntity dengan pesan sukses, pesan tanpa perubahan, atau respons kesalahan
     */
    public ResponseEntity<Map<String, Object>> updateStatus(Status status) {
        try {
            Map<String, Object> updateData = status.toMap();
            updateData.remove("uid");
            // Menghapus data yang tidak berubah
            Map<String, Object> filteredData = new HashMap<>();
            for (Map.Entry<String, Object> entry : updateData.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();

                // Memastikan nilai tidak null
                if (value != null) {
                    filteredData.put(key, value);
                }
            }

            // Melakukan pembaruan hanya jika ada perubahan
            if (!filteredData.isEmpty()) {
                filteredData.put("lastUpdated", FieldValue.serverTimestamp()); // Opsional: Sertakan field timestamp

                dbFirestore.collection(COLLECTION_NAME)
                        .document(status.getUid())
                        .update(filteredData);

                Map<String, Object> response = new HashMap<>();
                response.put("code", "200");
                response.put("status", "OK");
                response.put("message", "Status berhasil diperbarui");

                return ResponseEntity.ok(response);
            }

            Map<String, Object> response = new HashMap<>();
            response.put("code", "200");
            response.put("status", "OK");
            response.put("message", "Tidak ada perubahan untuk diperbarui");

            return ResponseEntity.ok(response);
        } catch (Exception e) {
            Map<String, Object> response = new HashMap<>();
            response.put("code", "500");
            response.put("status", "Internal Server Error");
            response.put("errors", e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    /**
     * Menghapus status berdasarkan UID.
     *
     * @param uid UID status
     * @return ResponseEntity dengan pesan sukses atau respons kesalahan
     */
    public ResponseEntity<Map<String, Object>> deleteStatus(String uid) {
        try {
            dbFirestore.collection(COLLECTION_NAME)
                    .document(uid)
                    .delete();

            Map<String, Object> response = new HashMap<>();
            response.put("code", "200");
            response.put("status", "OK");
            response.put("message", "Status berhasil dihapus");

            return ResponseEntity.ok(response);
        } catch (Exception e) {
            Map<String, Object> response = new HashMap<>();
            response.put("code", "500");
            response.put("status", "Internal Server Error");
            response.put("errors", e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    /**
     * Mengambil semua status.
     *
     * @return ResponseEntity dengan daftar status atau respons kesalahan
     */
    public ResponseEntity<Map<String, Object>> getAllStatuss() {
        try {
            CollectionReference collectionReference = dbFirestore.collection(COLLECTION_NAME);
            ApiFuture<QuerySnapshot> future = collectionReference.get();
            QuerySnapshot querySnapshot = future.get();
            List<Status> statusList = new ArrayList<>();

            for (QueryDocumentSnapshot document : querySnapshot.getDocuments()) {
                Status status = document.toObject(Status.class);
                statusList.add(status);
            }

            Map<String, Object> response = new HashMap<>();
            response.put("code", "200");
            response.put("status", "OK");
            response.put("data", statusList);

            return ResponseEntity.ok(response);
        } catch (Exception e) {
            Map<String, Object> response = new HashMap<>();
            response.put("code", "500");
            response.put("status", "Internal Server Error");
            response.put("errors", e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }
}
