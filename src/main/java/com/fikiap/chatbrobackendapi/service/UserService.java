package com.fikiap.chatbrobackendapi.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fikiap.chatbrobackendapi.config.DatabaseConnection;
import com.fikiap.chatbrobackendapi.entity.User;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.FieldValue;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;

/**
 * Kelas Service untuk mengelola operasi terkait pengguna.
 */
@Service
public class UserService {

    private static final String COLLECTION_NAME = "users";
    private final Firestore dbFirestore;

    public UserService(DatabaseConnection databaseConnection) {
        this.dbFirestore = databaseConnection.getFirestore();
    }

    /**
     * Menyimpan pengguna ke database.
     *
     * @param user Pengguna yang akan disimpan
     * @return ResponseEntity dengan respons API yang sesuai
     */
    public ResponseEntity<Map<String, Object>> saveUser(User user) {
        if (user == null || StringUtils.isEmpty(user.getUid())) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "400");
            errorResponse.put("status", "Bad Request");
            errorResponse.put("message", "Data pengguna tidak valid");

            return ResponseEntity.badRequest().body(errorResponse);
        }

        try {
            dbFirestore.collection(COLLECTION_NAME)
                    .document(user.getUid())
                    .set(user);

            Map<String, Object> successResponse = new HashMap<>();
            successResponse.put("code", "200");
            successResponse.put("status", "OK");
            successResponse.put("message", "Pengguna berhasil disimpan");

            return ResponseEntity.ok(successResponse);
        } catch (Exception e) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "500");
            errorResponse.put("status", "Internal Server Error");
            errorResponse.put("message", "Gagal menyimpan pengguna: " + e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
        }
    }

    /**
     * Mengambil detail pengguna berdasarkan ID pengguna.
     *
     * @param uid ID pengguna
     * @return ResponseEntity dengan respons API yang sesuai
     */
    public ResponseEntity<Map<String, Object>> getUserDetails(String uid) {
        if (StringUtils.isEmpty(uid)) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "400");
            errorResponse.put("status", "Bad Request");
            errorResponse.put("message", "ID pengguna tidak valid");

            return ResponseEntity.badRequest().body(errorResponse);
        }

        try {
            DocumentReference documentReference = dbFirestore.collection(COLLECTION_NAME).document(uid);
            ApiFuture<DocumentSnapshot> future = documentReference.get();
            DocumentSnapshot documentSnapshot = future.get();

            if (documentSnapshot.exists()) {
                User user = documentSnapshot.toObject(User.class);

                Map<String, Object> successResponse = new HashMap<>();
                successResponse.put("code", "200");
                successResponse.put("status", "OK");
                successResponse.put("data", user);

                return ResponseEntity.ok(successResponse);
            } else {
                Map<String, Object> errorResponse = new HashMap<>();
                errorResponse.put("code", "404");
                errorResponse.put("status", "Not Found");
                errorResponse.put("message", "Pengguna tidak ditemukan");

                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
            }
        } catch (Exception e) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "500");
            errorResponse.put("status", "Internal Server Error");
            errorResponse.put("message", "Gagal mengambil detail pengguna: " + e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
        }
    }

    /**
     * Memperbarui pengguna di database.
     *
     * @param user Data pengguna yang diperbarui
     * @return ResponseEntity dengan respons API yang sesuai
     */
    public ResponseEntity<Map<String, Object>> updateUser(User user) {
        if (user == null || StringUtils.isEmpty(user.getUid())) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "400");
            errorResponse.put("status", "Bad Request");
            errorResponse.put("message", "Data pengguna tidak valid");

            return ResponseEntity.badRequest().body(errorResponse);
        }

        try {
            Map<String, Object> updateData = user.toMap();
            updateData.remove("uid"); // Hapus field UID dari data pembaruan

            // Filter data yang tidak berubah
            Map<String, Object> filteredData = new HashMap<>();
            for (Map.Entry<String, Object> entry : updateData.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();

                // Pastikan nilai tidak null
                if (value != null) {
                    filteredData.put(key, value);
                }
            }

            // Lakukan pembaruan hanya jika ada perubahan
            if (!filteredData.isEmpty()) {
                filteredData.put("lastUpdated", FieldValue.serverTimestamp()); // Opsional: Sertakan field timestamp

                dbFirestore.collection(COLLECTION_NAME)
                        .document(user.getUid())
                        .update(filteredData);

                Map<String, Object> successResponse = new HashMap<>();
                successResponse.put("code", "200");
                successResponse.put("status", "OK");
                successResponse.put("message", "Pengguna berhasil diperbarui");

                return ResponseEntity.ok(successResponse);
            }

            Map<String, Object> successResponse = new HashMap<>();
            successResponse.put("code", "200");
            successResponse.put("status", "OK");
            successResponse.put("message", "Tidak ada perubahan untuk diperbarui");

            return ResponseEntity.ok(successResponse);
        } catch (Exception e) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "500");
            errorResponse.put("status", "Internal Server Error");
            errorResponse.put("message", "Gagal memperbarui pengguna: " + e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
        }
    }

    /**
     * Menghapus pengguna dari database berdasarkan ID pengguna.
     *
     * @param uid ID pengguna
     * @return ResponseEntity dengan respons API yang sesuai
     */
    public ResponseEntity<Map<String, Object>> deleteUser(String uid) {
        if (StringUtils.isEmpty(uid)) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "400");
            errorResponse.put("status", "Bad Request");
            errorResponse.put("message", "ID pengguna tidak valid");

            return ResponseEntity.badRequest().body(errorResponse);
        }

        try {
            dbFirestore.collection(COLLECTION_NAME)
                    .document(uid)
                    .delete();

            Map<String, Object> successResponse = new HashMap<>();
            successResponse.put("code", "200");
            successResponse.put("status", "OK");
            successResponse.put("message", "Pengguna berhasil dihapus");

            return ResponseEntity.ok(successResponse);
        } catch (Exception e) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "500");
            errorResponse.put("status", "Internal Server Error");
            errorResponse.put("message", "Gagal menghapus pengguna: " + e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
        }
    }

    /**
     * Mengambil semua pengguna dari database.
     *
     * @return ResponseEntity dengan respons API yang sesuai
     */
    public ResponseEntity<Map<String, Object>> getAllUsers() {
        try {
            CollectionReference collectionReference = dbFirestore.collection(COLLECTION_NAME);
            ApiFuture<QuerySnapshot> future = collectionReference.get();
            QuerySnapshot querySnapshot = future.get();
            List<User> userList = new ArrayList<>();

            for (QueryDocumentSnapshot document : querySnapshot.getDocuments()) {
                User user = document.toObject(User.class);
                userList.add(user);
            }

            Map<String, Object> successResponse = new HashMap<>();
            successResponse.put("code", "200");
            successResponse.put("status", "OK");
            successResponse.put("data", userList);

            return ResponseEntity.ok(successResponse);
        } catch (Exception e) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "500");
            errorResponse.put("status", "Internal Server Error");
            errorResponse.put("message", "Gagal mengambil pengguna: " + e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
        }
    }
}
