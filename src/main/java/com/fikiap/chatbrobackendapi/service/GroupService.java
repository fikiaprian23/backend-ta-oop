package com.fikiap.chatbrobackendapi.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fikiap.chatbrobackendapi.config.DatabaseConnection;
import com.fikiap.chatbrobackendapi.entity.Group;
import com.fikiap.chatbrobackendapi.entity.Message;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.FieldValue;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;

/**
 * Kelas Service untuk mengelola operasi terkait grup.
 */
@Service
public class GroupService {

    private static final String COLLECTION_NAME = "groups";
    private final Firestore dbFirestore;

    public GroupService(DatabaseConnection databaseConnection) {
        this.dbFirestore = databaseConnection.getFirestore();
    }

    /**
     * Menyimpan grup ke database.
     *
     * @param group Grup yang akan disimpan
     * @return ResponseEntity dengan respons API yang sesuai
     */
    public ResponseEntity<Map<String, Object>> saveGroup(Group group) {
        if (group == null || StringUtils.isEmpty(group.getGroupId())) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "400");
            errorResponse.put("status", "Bad Request");
            errorResponse.put("message", "Data grup tidak valid");

            return ResponseEntity.badRequest().body(errorResponse);
        }

        try {
            dbFirestore.collection(COLLECTION_NAME)
                    .document(group.getGroupId())
                    .set(group);

            Map<String, Object> successResponse = new HashMap<>();
            successResponse.put("code", "200");
            successResponse.put("status", "OK");
            successResponse.put("message", "Grup berhasil disimpan");

            return ResponseEntity.ok(successResponse);
        } catch (Exception e) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "500");
            errorResponse.put("status", "Internal Server Error");
            errorResponse.put("message", "Gagal menyimpan grup: " + e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
        }
    }

    /**
     * Mengambil detail grup berdasarkan ID grup.
     *
     * @param groupId ID grup
     * @return ResponseEntity dengan respons API yang sesuai
     */
    public ResponseEntity<Map<String, Object>> getGroupDetails(String groupId) {
        if (StringUtils.isEmpty(groupId)) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "400");
            errorResponse.put("status", "Bad Request");
            errorResponse.put("message", "ID grup tidak valid");

            return ResponseEntity.badRequest().body(errorResponse);
        }

        try {
            DocumentReference documentReference = dbFirestore.collection(COLLECTION_NAME).document(groupId);
            ApiFuture<DocumentSnapshot> future = documentReference.get();
            DocumentSnapshot documentSnapshot = future.get();

            if (documentSnapshot.exists()) {
                Group group = documentSnapshot.toObject(Group.class);

                Map<String, Object> successResponse = new HashMap<>();
                successResponse.put("code", "200");
                successResponse.put("status", "OK");
                successResponse.put("data", group);

                return ResponseEntity.ok(successResponse);
            } else {
                Map<String, Object> errorResponse = new HashMap<>();
                errorResponse.put("code", "404");
                errorResponse.put("status", "Not Found");
                errorResponse.put("message", "Grup tidak ditemukan");

                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
            }
        } catch (Exception e) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "500");
            errorResponse.put("status", "Internal Server Error");
            errorResponse.put("message", "Gagal mengambil detail grup: " + e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
        }
    }

    /**
     * Memperbarui grup di database.
     *
     * @param group Data grup yang diperbarui
     * @return ResponseEntity dengan respons API yang sesuai
     */
    public ResponseEntity<Map<String, Object>> updateGroup(Group group) {
        if (group == null || StringUtils.isEmpty(group.getGroupId())) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "400");
            errorResponse.put("status", "Bad Request");
            errorResponse.put("message", "Data grup tidak valid");

            return ResponseEntity.badRequest().body(errorResponse);
        }

        try {
            Map<String, Object> updateData = group.toMap();
            updateData.remove("groupId"); // Hapus field GroupId dari data pembaruan

            // Filter data yang tidak berubah
            Map<String, Object> filteredData = new HashMap<>();
            for (Map.Entry<String, Object> entry : updateData.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();

                // Pastikan nilai tidak null
                if (value != null) {
                    filteredData.put(key, value);
                }
            }

            // Lakukan pembaruan hanya jika ada perubahan
            if (!filteredData.isEmpty()) {
                filteredData.put("lastUpdated", FieldValue.serverTimestamp()); // Opsional: Sertakan field timestamp

                dbFirestore.collection(COLLECTION_NAME)
                        .document(group.getGroupId())
                        .update(filteredData);

                Map<String, Object> successResponse = new HashMap<>();
                successResponse.put("code", "200");
                successResponse.put("status", "OK");
                successResponse.put("message", "Grup berhasil diperbarui");

                return ResponseEntity.ok(successResponse);
            }

            Map<String, Object> successResponse = new HashMap<>();
            successResponse.put("code", "200");
            successResponse.put("status", "OK");
            successResponse.put("message", "Tidak ada perubahan untuk diperbarui");

            return ResponseEntity.ok(successResponse);
        } catch (Exception e) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "500");
            errorResponse.put("status", "Internal Server Error");
            errorResponse.put("message", "Gagal memperbarui grup: " + e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
        }
    }

    /**
     * Menghapus grup dari database berdasarkan ID grup.
     *
     * @param groupId ID grup
     * @return ResponseEntity dengan respons API yang sesuai
     */
    public ResponseEntity<Map<String, Object>> deleteGroup(String groupId) {
        if (StringUtils.isEmpty(groupId)) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "400");
            errorResponse.put("status", "Bad Request");
            errorResponse.put("message", "ID grup tidak valid");

            return ResponseEntity.badRequest().body(errorResponse);
        }

        try {
            dbFirestore.collection(COLLECTION_NAME)
                    .document(groupId)
                    .delete();

            Map<String, Object> successResponse = new HashMap<>();
            successResponse.put("code", "200");
            successResponse.put("status", "OK");
            successResponse.put("message", "Grup berhasil dihapus");

            return ResponseEntity.ok(successResponse);
        } catch (Exception e) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "500");
            errorResponse.put("status", "Internal Server Error");
            errorResponse.put("message", "Gagal menghapus grup: " + e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
        }
    }

    /**
     * Mengambil semua grup dari database.
     *
     * @return ResponseEntity dengan respons API yang sesuai
     */
    public ResponseEntity<Map<String, Object>> getAllGroups() {
        try {
            CollectionReference collectionReference = dbFirestore.collection(COLLECTION_NAME);
            ApiFuture<QuerySnapshot> future = collectionReference.get();
            QuerySnapshot querySnapshot = future.get();
            List<Group> groupList = new ArrayList<>();

            for (QueryDocumentSnapshot document : querySnapshot.getDocuments()) {
                Group group = document.toObject(Group.class);
                groupList.add(group);
            }

            Map<String, Object> successResponse = new HashMap<>();
            successResponse.put("code", "200");
            successResponse.put("status", "OK");
            successResponse.put("data", groupList);

            return ResponseEntity.ok(successResponse);
        } catch (Exception e) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "500");
            errorResponse.put("status", "Internal Server Error");
            errorResponse.put("message", "Gagal mengambil grup: " + e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
        }
    }
    
    /**
     * Menyimpan pesan ke dalam sub-koleksi "chats" di bawah grup.
     *
     * @param groupId ID grup
     * @param message Pesan yang akan disimpan
     * @return ResponseEntity dengan respons API yang sesuai
     */
    public ResponseEntity<Map<String, Object>> saveMessage(String groupId, Message message) {
        if (StringUtils.isEmpty(groupId) || message == null) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "400");
            errorResponse.put("status", "Bad Request");
            errorResponse.put("message", "Data grup atau pesan tidak valid");

            return ResponseEntity.badRequest().body(errorResponse);
        }

        try {
            DocumentReference groupReference = dbFirestore.collection(COLLECTION_NAME).document(groupId);
            CollectionReference chatCollection = groupReference.collection("chats");

            // Simpan pesan ke dalam sub-koleksi "chats" dengan menggunakan messageId sebagai nama dokumen
            chatCollection.document(message.getMessageId()).set(message);

            Map<String, Object> successResponse = new HashMap<>();
            successResponse.put("code", "200");
            successResponse.put("status", "OK");
            successResponse.put("message", "Pesan berhasil disimpan");

            return ResponseEntity.ok(successResponse);
        } catch (Exception e) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "500");
            errorResponse.put("status", "Internal Server Error");
            errorResponse.put("message", "Gagal menyimpan pesan: " + e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
        }
    }

}
