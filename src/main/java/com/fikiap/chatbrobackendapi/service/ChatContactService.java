package com.fikiap.chatbrobackendapi.service;

import com.fikiap.chatbrobackendapi.config.DatabaseConnection;
import com.fikiap.chatbrobackendapi.entity.ChatContact;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;

import jakarta.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Layanan untuk mengelola operasi terkait Chat Contact.
 */
@Service
public class ChatContactService {
    private static final String USERS_COLLECTION_NAME = "users";
    private static final String CHAT_CONTACTS_COLLECTION_NAME = "chats";
    private final Firestore dbFirestore;

    public ChatContactService(DatabaseConnection databaseConnection) {
        this.dbFirestore = databaseConnection.getFirestore();
    }

    /**
     * Menyimpan kontak chat baru.
     *
     * @param userId      ID pengguna
     * @param chatContact Kontak chat yang akan disimpan
     * @return ResponseEntity dengan pesan sukses atau pesan kesalahan
     */
    public ResponseEntity<Map<String, Object>> saveChatContact(String userId, @Valid ChatContact chatContact) {
        try {
            dbFirestore.collection(USERS_COLLECTION_NAME)
                    .document(userId)
                    .collection(CHAT_CONTACTS_COLLECTION_NAME)
                    .document(chatContact.getContactId())
                    .set(chatContact.toMap());

            Map<String, Object> response = new HashMap<>();
            response.put("code", "200");
            response.put("status", "OK");
            response.put("data", chatContact);

            return ResponseEntity.ok(response);
        } catch (Exception e) {
            Map<String, Object> response = new HashMap<>();
            response.put("code", "500");
            response.put("status", "Internal Server Error");
            response.put("errors", e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    /**
     * Mengambil detail kontak chat berdasarkan ID pengguna dan ID kontak.
     *
     * @param userId    ID pengguna
     * @param contactId ID kontak
     * @return ResponseEntity dengan detail kontak chat atau respons not found
     */
    public ResponseEntity<Map<String, Object>> getChatContactDetails(String userId, String contactId) {
        try {
            DocumentReference documentReference = dbFirestore.collection(USERS_COLLECTION_NAME)
                    .document(userId)
                    .collection(CHAT_CONTACTS_COLLECTION_NAME)
                    .document(contactId);
            ApiFuture<DocumentSnapshot> future = documentReference.get();
            DocumentSnapshot documentSnapshot = future.get();

            if (documentSnapshot.exists()) {
                ChatContact chatContact = documentSnapshot.toObject(ChatContact.class);

                Map<String, Object> response = new HashMap<>();
                response.put("code", "200");
                response.put("status", "OK");
                response.put("data", chatContact);

                return ResponseEntity.ok(response);
            } else {
                Map<String, Object> response = new HashMap<>();
                response.put("code", "404");
                response.put("status", "Not Found");

                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
            }
        } catch (Exception e) {
            Map<String, Object> response = new HashMap<>();
           response.put("code", "500");
            response.put("status", "Internal Server Error");
            response.put("errors", e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    /**
     * Memperbarui kontak chat berdasarkan ID pengguna dan ID kontak.
     *
     * @param userId      ID pengguna
     * @param contactId   ID kontak
     * @param chatContact Kontak chat yang diperbarui
     * @return ResponseEntity dengan pesan sukses, pesan tanpa perubahan, atau pesan kesalahan
     */
    public ResponseEntity<Map<String, Object>> updateChatContact(String userId, String contactId, @Valid ChatContact chatContact) {
        try {
            Map<String, Object> updateData = chatContact.toMap();
            updateData.remove("contactId"); // Menghapus field contactId dari data pembaruan

            // Menghapus data yang tidak berubah
            Map<String, Object> filteredData = new HashMap<>();
            for (Map.Entry<String, Object> entry : updateData.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();

                // Memastikan nilai tidak null
                if (value != null) {
                    filteredData.put(key, value);
                }
            }

            // Melakukan pembaruan hanya jika ada perubahan
            if (!filteredData.isEmpty()) {
                dbFirestore.collection(USERS_COLLECTION_NAME)
                        .document(userId)
                        .collection(CHAT_CONTACTS_COLLECTION_NAME)
                        .document(contactId)
                        .update(filteredData);

                Map<String, Object> response = new HashMap<>();
                response.put("code", "200");
                response.put("status", "OK");
                response.put("message", "Chat contact updated successfully");

                return ResponseEntity.ok(response);
            }

            Map<String, Object> response = new HashMap<>();
            response.put("code", "200");
            response.put("status", "OK");
            response.put("message", "No changes to update");

            return ResponseEntity.ok(response);
        } catch (Exception e) {
            Map<String, Object> response = new HashMap<>();
            response.put("code", "500");
            response.put("status", "Internal Server Error");
            response.put("errors", e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    /**
     * Menghapus kontak chat berdasarkan ID pengguna dan ID kontak.
     *
     * @param userId    ID pengguna
     * @param contactId ID kontak
     * @return ResponseEntity dengan pesan sukses atau pesan kesalahan
     */
    public ResponseEntity<Map<String, Object>> deleteChatContact(String userId, String contactId) {
        try {
            dbFirestore.collection(USERS_COLLECTION_NAME)
                    .document(userId)
                    .collection(CHAT_CONTACTS_COLLECTION_NAME)
                    .document(contactId)
                    .delete();

            Map<String, Object> response = new HashMap<>();
            response.put("code", "200");
            response.put("status", "OK");
            response.put("message", "Chat contact deleted successfully");

            return ResponseEntity.ok(response);
        } catch (Exception e) {
            Map<String, Object> response = new HashMap<>();
            response.put("code", "500");
            response.put("status", "Internal Server Error");
            response.put("errors", e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    /**
     * Mengambil daftar semua kontak chat berdasarkan ID pengguna.
     *
     * @param userId ID pengguna
     * @return ResponseEntity dengan daftar kontak chat atau pesankesalahan
     */
    public ResponseEntity<Map<String, Object>> getAllChatContacts(String userId) {
        try {
            CollectionReference collectionReference = dbFirestore.collection(USERS_COLLECTION_NAME)
                    .document(userId)
                    .collection(CHAT_CONTACTS_COLLECTION_NAME);
            ApiFuture<QuerySnapshot> future = collectionReference.get();
            QuerySnapshot querySnapshot = future.get();
            List<ChatContact> chatContactList = new ArrayList<>();

            for (QueryDocumentSnapshot document : querySnapshot.getDocuments()) {
                ChatContact chatContact = document.toObject(ChatContact.class);
                chatContactList.add(chatContact);
            }

            Map<String, Object> response = new HashMap<>();
            response.put("code", "200");
            response.put("status", "OK");
            response.put("data", chatContactList);

            return ResponseEntity.ok(response);
        } catch (Exception e) {
            Map<String, Object> response = new HashMap<>();
            response.put("code", "500");
            response.put("status", "Internal Server Error");
            response.put("errors", e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }
}
