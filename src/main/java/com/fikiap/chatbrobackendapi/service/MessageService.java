package com.fikiap.chatbrobackendapi.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fikiap.chatbrobackendapi.config.DatabaseConnection;
import com.fikiap.chatbrobackendapi.entity.Message;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;

@Service
public class MessageService {
private static final String USERS_COLLECTION_NAME = "users";
private static final String MESSAGE_SUBCOLLECTION_NAME = "messages";
private final Firestore dbFirestore;
public MessageService(DatabaseConnection databaseConnection) {
    this.dbFirestore = databaseConnection.getFirestore();
}

/**
 * Menyimpan pesan dalam koleksi pesan di dalam dokumen kontak pengguna.
 *
 * @param userId    ID pengguna
 * @param contactId ID kontak pengguna
 * @param message   Objek Message yang akan disimpan
 * @return ResponseEntity dengan pesan sukses atau pesan kesalahan
 */
public ResponseEntity<Map<String, Object>> saveMessage(String userId, String contactId, Message message) {
    try {
        DocumentReference contactDocument = dbFirestore.collection(USERS_COLLECTION_NAME)
                .document(userId)
                .collection("chats")
                .document(contactId);

        // Buat subkoleksi MESSAGE_SUBCOLLECTION_NAME di dalam dokumen kontak
        CollectionReference messageCollection = contactDocument.collection(MESSAGE_SUBCOLLECTION_NAME);

        // Simpan pesan di dalam subkoleksi MESSAGE_SUBCOLLECTION_NAME dengan menggunakan messageId sebagai nama dokumen
        DocumentReference messageDocument = messageCollection.document(message.getMessageId());
        messageDocument.set(message);

        Map<String, Object> response = new HashMap<>();
        response.put("code", "200");
        response.put("status", "OK");
        response.put("data", message);

        return ResponseEntity.ok(response);
    } catch (Exception e) {
        return createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
    }
}

/**
 * Mengambil detail pesan dari koleksi pesan di dalam dokumen kontak pengguna.
 *
 * @param userId    ID pengguna
 * @param contactId ID kontak pengguna
 * @return ResponseEntity dengan daftar pesan atau pesan kesalahan
 */
public ResponseEntity<Map<String, Object>> getMessageDetails(String userId, String contactId) {
    try {
        DocumentReference contactDocument = dbFirestore.collection(USERS_COLLECTION_NAME)
                .document(userId)
                .collection("chats")
                .document(contactId);

        // Dapatkan subkoleksi MESSAGE_SUBCOLLECTION_NAME di dalam dokumen kontak
        CollectionReference messageCollection = contactDocument.collection(MESSAGE_SUBCOLLECTION_NAME);

        // Dapatkan semua dokumen pesan dari subkoleksi MESSAGE_SUBCOLLECTION_NAME
        ApiFuture<QuerySnapshot> future = messageCollection.get();
        QuerySnapshot querySnapshot = future.get();
        List<Message> messageList = new ArrayList<>();

        for (QueryDocumentSnapshot document : querySnapshot.getDocuments()) {
            Message message = document.toObject(Message.class);
            messageList.add(message);
        }

        Map<String, Object> response = new HashMap<>();
        response.put("code", "200");
        response.put("status", "OK");
        response.put("data", messageList);

        return ResponseEntity.ok(response);
    } catch (Exception e) {
        return createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
    }
}

/**
 * Memperbarui pesan dalam koleksi pesan di dalam dokumen kontak pengguna.
 *
 * @param userId    ID pengguna
 * @param contactId ID kontak pengguna
 * @param message   Objek Message yang akan diperbarui
 * @return ResponseEntity dengan pesan sukses, pesan tanpa perubahan, atau pesan kesalahan
 */
public ResponseEntity<Map<String, Object>> updateMessage(String userId, String contactId, Message message) {
    try {
        DocumentReference contactDocument = dbFirestore.collection(USERS_COLLECTION_NAME)
                .document(userId)
                .collection("chats")
                .document(contactId);

        // Dapatkan subkoleksi MESSAGE_SUBCOLLECTION_NAME di dalam dokumen kontak
        CollectionReference messageCollection = contactDocument.collection(MESSAGE_SUBCOLLECTION_NAME);

        // ...

        Map<String, Object> response = new HashMap<>();
        response.put("code", "200");
        response.put("status", "OK");
        response.put(MESSAGE_SUBCOLLECTION_NAME, "Message updated successfully");

        return ResponseEntity.ok(response);
    } catch (Exception e) {
        return createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
    }
}

/**
 * Menghapus pesan dari koleksi pesan di dalam dokumen kontak pengguna.
 *
 * @param userId    ID pengguna
 * @param contactId ID kontak pengguna
 * @return ResponseEntity dengan pesan sukses atau pesan kesalahan
 */
public ResponseEntity<Map<String, Object>> deleteMessage(String userId, String contactId) {
    try {
        DocumentReference contactDocument = dbFirestore.collection(USERS_COLLECTION_NAME)
                .document(userId)
                .collection("chats")
                .document(contactId);

        // Hapus subkoleksi MESSAGE_SUBCOLLECTION_NAME di dalam dokumen kontak
        CollectionReference messageCollection = contactDocument.collection(MESSAGE_SUBCOLLECTION_NAME);
        ApiFuture<QuerySnapshot> future = messageCollection.get();
        QuerySnapshot querySnapshot = future.get();
        for (QueryDocumentSnapshot document : querySnapshot.getDocuments()) {
            document.getReference().delete();
        }

        Map<String, Object> response = new HashMap<>();
        response.put("code", "200");
        response.put("status", "OK");
        response.put(MESSAGE_SUBCOLLECTION_NAME, "Message deleted successfully");

        return ResponseEntity.ok(response);
    } catch (Exception e) {
        return createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
    }
}

/**
 * Mengambil semua pesan dari koleksi pesan di dalam dokumen kontak pengguna.
 *
 * @param userId ID pengguna
 * @return ResponseEntity dengan daftar pesan atau pesan kesalahan
 */
public ResponseEntity<Map<String, Object>> getAllMessages(String userId) {
    try {
        CollectionReference collectionReference = dbFirestore.collection(USERS_COLLECTION_NAME)
                .document(userId)
                .collection("chats");
        ApiFuture<QuerySnapshot> future = collectionReference.get();
        QuerySnapshot querySnapshot = future.get();
        Map<String, List<Message>> messageMap = new HashMap<>();

        for (QueryDocumentSnapshot document : querySnapshot.getDocuments()) {
            DocumentReference contactDocument = document.getReference();
            String contactId = document.getId();

            // Dapatkan subkoleksi MESSAGE_SUBCOLLECTION_NAME di dalam dokumen kontak
            CollectionReference messageCollection = contactDocument.collection(MESSAGE_SUBCOLLECTION_NAME);

            // Dapatkan semua dokumen pesan dari subkoleksi MESSAGE_SUB                
            ApiFuture<QuerySnapshot> messageFuture = messageCollection.get();
            QuerySnapshot messageQuerySnapshot = messageFuture.get();
            List<Message> messageList = new ArrayList<>();

            for (QueryDocumentSnapshot messageDocument : messageQuerySnapshot.getDocuments()) {
                Message message = messageDocument.toObject(Message.class);
                messageList.add(message);
            }

            messageMap.put(contactId, messageList);
        }

        Map<String, Object> response = new HashMap<>();
        response.put("code", "200");
        response.put("status", "OK");
        response.put("data", messageMap);

        return ResponseEntity.ok(response);
    } catch (Exception e) {
        return createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
    }
}

/**
 * Membuat ResponseEntity dengan respons kesalahan.
 *
 * @param status       Status HTTP yang akan digunakan dalam respons
 * @param errorMessage Pesan kesalahan
 * @return ResponseEntity dengan pesan kesalahan
 */
private ResponseEntity<Map<String, Object>> createErrorResponse(HttpStatus status, String errorMessage) {
    Map<String, Object> response = new HashMap<>();
    response.put("code", status.toString());
    response.put("status", status.getReasonPhrase());
    response.put("errors", errorMessage);

    return ResponseEntity.status(status).body(response);
}
}