package com.fikiap.chatbrobackendapi.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fikiap.chatbrobackendapi.config.DatabaseConnection;
import com.fikiap.chatbrobackendapi.entity.Admin;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.FieldValue;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;

/**
 * Kelas Service untuk mengelola operasi terkait admin.
 */
@Service
public class AdminService {
    private static final String COLLECTION_NAME = "admins";
    private final Firestore dbFirestore;

    public AdminService(DatabaseConnection databaseConnection) {
        this.dbFirestore = databaseConnection.getFirestore();
    }

    /**
     * Menyimpan admin ke database.
     *
     * @param admin Admin yang akan disimpan
     * @return ResponseEntity dengan format respons API     
     */
    public ResponseEntity<Map<String, Object>> saveAdmin(Admin admin) {
        try {
            dbFirestore.collection(COLLECTION_NAME)
                    .document(admin.getUid())
                    .set(admin);

            Map<String, Object> responseData = new HashMap<>();
            responseData.put("code", "200");
            responseData.put("status", "OK");
            responseData.put("data", admin);

            return ResponseEntity.ok(responseData);
        } catch (Exception e) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "500");
            errorResponse.put("status", "Internal Server Error");
            errorResponse.put("errors", e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
        }
    }

    /**
     * Mengambil detail admin.
     *
     * @param uid ID admin
     * @return ResponseEntity dengan format respons API
     */
    public ResponseEntity<Map<String, Object>> getAdminDetails(String uid) {
        try {
            DocumentReference documentReference = dbFirestore.collection(COLLECTION_NAME).document(uid);
            ApiFuture<DocumentSnapshot> future = documentReference.get();
            DocumentSnapshot documentSnapshot = future.get();

            if (documentSnapshot.exists()) {
                Admin admin = documentSnapshot.toObject(Admin.class);

                Map<String, Object> responseData = new HashMap<>();
                responseData.put("code", "200");
                responseData.put("status", "OK");
                responseData.put("data", admin);

                return ResponseEntity.ok(responseData);
            } else {
                Map<String, Object> errorResponse = new HashMap<>();
                errorResponse.put("code", "404");
                errorResponse.put("status", "Not Found");
                errorResponse.put("errors", "Admin not found");

                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
            }
        } catch (Exception e) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "500");
            errorResponse.put("status", "Internal Server Error");
            errorResponse.put("errors", e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
        }
    }

    /**
     * Memperbarui admin.
     *
     * @param admin Admin yang akan diperbarui
     * @return ResponseEntity dengan format respons API
     */
    public ResponseEntity<Map<String, Object>> updateAdmin(Admin admin) {
        try {
            Map<String, Object> updateData = admin.toMap();
            updateData.remove("uid"); // Menghapus field UID dari data pembaruan

            // Menghapus data yang tidak berubah
            Map<String, Object> filteredData = new HashMap<>();
            for (Map.Entry<String, Object> entry : updateData.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();

                // Memastikan nilai tidak null
                if (value != null) {
                    filteredData.put(key, value);
                }
            }

            // Melakukan pembaruan hanya jika ada perubahan
            if (!filteredData.isEmpty()) {
                filteredData.put("lastUpdated", FieldValue.serverTimestamp()); // Opsional: Sertakan field timestamp

                dbFirestore.collection(COLLECTION_NAME)
                        .document(admin.getUid())
                        .update(filteredData);

                Map<String, Object> responseData = new HashMap<>();
                responseData.put("code", "200");
                responseData.put("status", "OK");
                responseData.put("data", admin);

                return ResponseEntity.ok(responseData);
            }

            Map<String, Object> responseData = new HashMap<>();
            responseData.put("code", "200");
            responseData.put("status", "OK");
            responseData.put("errors", new HashMap<>());

            return ResponseEntity.ok(responseData);
        } catch (Exception e) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "500");
            errorResponse.put("status", "Internal Server Error");
            errorResponse.put("errors", e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
        }
    }

    /**
     * Menghapus admin dari database.
     *
     * @param uid ID admin yang akan dihapus
     * @return ResponseEntity dengan format respons API
     */
    public ResponseEntity<Map<String, Object>> deleteAdmin(String uid) {
        try {
            dbFirestore.collection(COLLECTION_NAME)
                    .document(uid)
                    .delete();

            Map<String, Object> responseData = new HashMap<>();
            responseData.put("code", "200");
            responseData.put("status", "OK");
            responseData.put("data", new HashMap<>());

            return ResponseEntity.ok(responseData);
        } catch (Exception e) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "500");
            errorResponse.put("status", "Internal Server Error");
            errorResponse.put("errors", "Failed to delete admin");

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
        }
    }

    /**
     * Mengambil semua admin dari database.
     *
     * @return ResponseEntity dengan format respons API
     */
    public ResponseEntity<Map<String, Object>> getAllAdmins() {
        try {
            CollectionReference collectionReference = dbFirestore.collection(COLLECTION_NAME);
            ApiFuture<QuerySnapshot> future = collectionReference.get();
            QuerySnapshot querySnapshot = future.get();
            List<Admin> adminList = new ArrayList<>();

            for (QueryDocumentSnapshot document : querySnapshot.getDocuments()) {
                Admin admin = document.toObject(Admin.class);
                adminList.add(admin);
            }

            Map<String, Object> responseData = new HashMap<>();
            responseData.put("code", "200");
            responseData.put("status", "OK");
            responseData.put("data", adminList);

            return ResponseEntity.ok(responseData);
        } catch (Exception e) {
            Map<String, Object> errorResponse = new HashMap<>();
            errorResponse.put("code", "500");
            errorResponse.put("status", "Internal Server Error");
            errorResponse.put("errors", "Failed to retrieve admins");

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
        }
    }
}
