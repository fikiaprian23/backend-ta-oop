package com.fikiap.chatbrobackendapi.controller;

import com.fikiap.chatbrobackendapi.entity.ChatContact;
import com.fikiap.chatbrobackendapi.entity.Message;
import com.fikiap.chatbrobackendapi.service.ChatContactService;
import com.fikiap.chatbrobackendapi.service.MessageService;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.Map;

/**
 * Controller untuk mengelola chat contacts dan messages.
 */
@RestController
@RequestMapping("/api/users")
public class ChatController {
    private final ChatContactService chatContactService;
    private final MessageService messageService;

    /**
     * Konstruktor untuk ChatController.
     *
     * @param chatContactService Layanan chat contact untuk menghandle operasi chat contact.
     * @param messageService     Layanan message untuk menghandle operasi message.
     */
    @Autowired
    public ChatController(ChatContactService chatContactService, MessageService messageService) {
        this.chatContactService = chatContactService;
        this.messageService = messageService;
    }

    // Chat Contacts

    /**
     * Membuat chat contact baru.
     *
     * @param userId      ID pengguna.
     * @param chatContact Informasi chat contact yang akan disimpan.
     * @return Respons HTTP yang berisi hasil operasi.
     */
    @PostMapping("/{userId}/chats")
    public ResponseEntity<Map<String, Object>> saveChatContact(
            @PathVariable String userId,
            @Valid @RequestBody ChatContact chatContact
    ) {
        return chatContactService.saveChatContact(userId, chatContact);
    }

    /**
     * Mendapatkan detail chat contact.
     *
     * @param userId     ID pengguna.
     * @param contactId  ID chat contact.
     * @return Respons HTTP yang berisi detail chat contact.
     */
    @GetMapping("/{userId}/chats/{contactId}")
    public ResponseEntity<Map<String, Object>> getChatContactDetails(
            @PathVariable String userId,
            @PathVariable String contactId
    ) {
        return chatContactService.getChatContactDetails(userId, contactId);
    }

    /**
     * Mengupdate chat contact.
     *
     * @param userId      ID pengguna.
     * @param contactId   ID chat contact.
     * @param chatContact Informasi chat contact yang akan diupdate.
     * @return Respons HTTP yang berisi hasil operasi.
     */
    @PutMapping("/{userId}/chats/{contactId}")
    public ResponseEntity<Map<String, Object>> updateChatContact(
            @PathVariable String userId,
            @PathVariable String contactId,
            @Valid @RequestBody ChatContact chatContact
    ) {
        chatContact.setContactId(contactId);
        return chatContactService.updateChatContact(userId, chatContact.getContactId(), chatContact);
    }

    /**
     * Menghapus chat contact.
     *
     * @param userId     ID pengguna.
     * @param contactId  ID chat contact yang akan dihapus.
     * @return Respons HTTP yang berisi hasil operasi.
     */
    @DeleteMapping("/{userId}/chats/{contactId}")
    public ResponseEntity<Map<String, Object>> deleteChatContact(
            @PathVariable String userId,
            @PathVariable String contactId
    ) {
        return chatContactService.deleteChatContact(userId, contactId);
    }

    /**
     * Mendapatkan semua chat contacts.
     *
     * @param userId ID pengguna.
     * @return Respons HTTP yang berisi daftar chat contacts.
     */
   @GetMapping("/{userId}/chats")
    public ResponseEntity<Map<String, Object>> getAllChatContacts(@PathVariable String userId) {
        return chatContactService.getAllChatContacts(userId);
    }

    // Messages

    /**
     * Menyimpan message baru ke dalam chat.
     *
     * @param userId     ID pengguna.
     * @param contactId  ID chat contact.
     * @param message    Message yang akan disimpan.
     * @return Respons HTTP yang berisi hasil operasi.
     */
    @PostMapping("/{userId}/chats/{contactId}/messages")
    public ResponseEntity<Map<String, Object>> saveMessage(
            @PathVariable String userId,
            @PathVariable String contactId,
            @Valid @RequestBody Message message
    ) {
        return messageService.saveMessage(userId, contactId, message);
    }

    /**
     * Mendapatkan detail message dalam chat.
     *
     * @param userId     ID pengguna.
     * @param contactId  ID chat contact.
     * @return Respons HTTP yang berisi detail message.
     */
    @GetMapping("/{userId}/chats/{contactId}/messages")
    public ResponseEntity<Map<String, Object>> getMessageDetails(
            @PathVariable String userId,
            @PathVariable String contactId
    ) {
        return messageService.getMessageDetails(userId, contactId);
    }

    /**
     * Mengupdate message dalam chat.
     *
     * @param userId     ID pengguna.
     * @param contactId  ID chat contact.
     * @param message    Message yang akan diupdate.
     * @return Respons HTTP yang berisi hasil operasi.
     */
    @PutMapping("/{userId}/chats/{contactId}/messages")
    public ResponseEntity<Map<String, Object>> updateMessage(
            @PathVariable String userId,
            @PathVariable String contactId,
            @Valid @RequestBody Message message
    ) {
        return messageService.updateMessage(userId, contactId, message);
    }

    /**
     * Menghapus message dalam chat.
     *
     * @param userId     ID pengguna.
     * @param contactId  ID chat contact.
     * @return Respons HTTP yang berisi hasil operasi.
     */
    @DeleteMapping("/{userId}/chats/{contactId}/messages")
    public ResponseEntity<Map<String, Object>> deleteMessage(
            @PathVariable String userId,
            @PathVariable String contactId
    ) {
        return messageService.deleteMessage(userId, contactId);
    }

    /**
     * Mendapatkan semua messages dalam chat.
     *
     * @param userId ID pengguna.
     * @return Respons HTTP yang berisi daftar messages.
     */
    @GetMapping("/{userId}/chats/messages")
    public ResponseEntity<Map<String, Object>> getAllMessages(
            @PathVariable String userId) {
        return messageService.getAllMessages(userId);
    }
}
