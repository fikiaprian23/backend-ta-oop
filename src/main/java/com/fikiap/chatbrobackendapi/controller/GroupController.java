package com.fikiap.chatbrobackendapi.controller;

import com.fikiap.chatbrobackendapi.entity.Group;
import com.fikiap.chatbrobackendapi.entity.Message;
import com.fikiap.chatbrobackendapi.service.GroupService;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.HashMap;
import java.util.Map;

/**
 * Controller untuk mengelola grup dan pesan.
 */
@RestController
@RequestMapping("/api/groups")
public class GroupController {

    private final GroupService groupService;

    @Autowired
    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

    /**
     * Membuat grup baru.
     *
     * @param group Informasi grup yang akan disimpan.
     * @return Respons HTTP yang berisi hasil operasi.
     */
    @PostMapping
    public ResponseEntity<Map<String, Object>> createGroup(@Valid @RequestBody Group group) {
        try {
            return groupService.saveGroup(group);
        } catch (Exception e) {
            return createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Gagal membuat grup: " + e.getMessage());
        }
    }

    /**
     * Mendapatkan detail grup.
     *
     * @param groupId ID grup.
     * @return Respons HTTP yang berisi detail grup.
     */
    @GetMapping("/{groupId}")
    public ResponseEntity<Map<String, Object>> getGroup(@PathVariable String groupId) {
        try {
            return groupService.getGroupDetails(groupId);
        } catch (Exception e) {
            return createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Gagal mengambil detail grup: " + e.getMessage());
        }
    }

    /**
     * Mengupdate grup.
     *
     * @param groupId ID grup.
     * @param group   Informasi grup yang akan diupdate.
     * @return Respons HTTP yang berisi hasil operasi.
     */
    @PutMapping("/{groupId}")
    public ResponseEntity<Map<String, Object>> updateGroup(@PathVariable String groupId, @Valid @RequestBody Group group) {
        try {
            group.setGroupId(groupId);
            return groupService.updateGroup(group);
        } catch (Exception e) {
            return createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Gagal memperbarui grup: " + e.getMessage());
        }
    }

    /**
     * Menghapus grup.
     *
     * @param groupId ID grup yang akan dihapus.
     * @return Respons HTTP yang berisi hasil operasi.
     */
    @DeleteMapping("/{groupId}")
    public ResponseEntity<Map<String, Object>> deleteGroup(@PathVariable String groupId) {
        try {
            return groupService.deleteGroup(groupId);
        } catch (Exception e) {
            return createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Gagal menghapus grup: " + e.getMessage());
        }
    }

    /**
     * Mendapatkan semua grup.
     *
     * @return Respons HTTP yang berisi daftar grup.
     */
    @GetMapping
    public ResponseEntity<Map<String, Object>> getAllGroups() {
        try {
            return groupService.getAllGroups();
        } catch (Exception e) {
            return createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Gagal mengambil grup: " + e.getMessage());
        }
    }
    
    /**
     * Menyimpan pesan ke dalam grup.
     *
     * @param groupId ID grup.
     * @param message Pesan yang akan disimpan.
     * @return Respons HTTP yang berisi hasil operasi.
     */
    @PostMapping("/{groupId}/chats")
    public ResponseEntity<Map<String, Object>> saveMessage(@PathVariable String groupId, @Valid @RequestBody Message message) {
        try {
            return groupService.saveMessage(groupId, message);
        } catch (Exception e) {
            return createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Gagal menyimpan pesan: " + e.getMessage());
        }
    }

    /**
     * Membuat respons kesalahan standar.
     *
     * @param httpStatus Status HTTP yang akan digunakan.
     * @param message    Pesan kesalahan.
     * @return Respons HTTP yang berisi informasi kesalahan.
     */
    private ResponseEntity<Map<String, Object>> createErrorResponse(HttpStatus httpStatus, String message) {
        Map<String, Object> errorResponse = new HashMap<>();
        errorResponse.put("code", httpStatus.value());
        errorResponse.put("status", httpStatus.getReasonPhrase());
        errorResponse.put("message", message);
        return ResponseEntity.status(httpStatus).body(errorResponse);
    }
}
