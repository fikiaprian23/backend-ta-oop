package com.fikiap.chatbrobackendapi.controller;

import com.fikiap.chatbrobackendapi.entity.User;
import com.fikiap.chatbrobackendapi.service.UserService;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



/**
 * Kelas Controller yang menangani permintaan terkait pengguna.
 */
@RestController
@RequestMapping("/api/users")
public class UserController {

	private final UserService userService;

	/**
	 * Konstruktor UserController.
	 *
	 * @param userService Layanan UserService yang digunakan.
	 */
	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}

	/**
	 * API untuk menyimpan pengguna baru.
	 *
	 * @param user Data pengguna yang akan disimpan
	 * @return ResponseEntity dengan pesan sukses atau pesan kesalahan
	 */
	@PostMapping
	public ResponseEntity<?> saveUser(@Valid @RequestBody User user) {
		try {
			return userService.saveUser(user);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body("Gagal menyimpan pengguna: " + e.getMessage());
		}
	}

	/**
	 * API untuk mendapatkan detail pengguna berdasarkan ID pengguna.
	 *
	 * @param uid ID pengguna
	 * @return ResponseEntity dengan detail pengguna atau respons not found
	 */
	@GetMapping("/{uid}")
	public ResponseEntity<?> getUser(@PathVariable String uid) {
		try {
			return userService.getUserDetails(uid);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body("Gagal mengambil detail pengguna: " + e.getMessage());
		}
	}

	/**
	 * API untuk memperbarui pengguna.
	 *
	 * @param uid  ID pengguna yang akan diperbarui
	 * @param user Data pengguna yang diperbarui
	 * @return ResponseEntity dengan pesan sukses, pesan tanpa perubahan, atau pesan
	 *         kesalahan
	 */
	@PutMapping("/{uid}")
	public ResponseEntity<?> updateUser(@PathVariable String uid, @Valid @RequestBody User user) {
		try {
			user.setUid(uid);
			return userService.updateUser(user);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body("Gagal memperbarui pengguna: " + e.getMessage());
		}
	}

	/**
	 * API untuk menghapus pengguna berdasarkan ID pengguna.
	 *
	 * @param uid ID pengguna
	 * @return ResponseEntity dengan pesan sukses atau pesan kesalahan
	 */
	@DeleteMapping("/{uid}")
	public ResponseEntity<?> deleteUser(@PathVariable String uid) {
		try {
			return userService.deleteUser(uid);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body("Gagal menghapus pengguna: " + e.getMessage());
		}
	}

	/**
	 * API untuk mendapatkan daftar semua pengguna.
	 *
	 * @return ResponseEntity dengan daftar pengguna atau pesan kesalahan
	 */
	@GetMapping
	public ResponseEntity<?> getAllUsers() {
		try {
			return userService.getAllUsers();
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body("Gagal mengambil pengguna: " + e.getMessage());
		}
	}
}