package com.fikiap.chatbrobackendapi.controller;

import com.fikiap.chatbrobackendapi.entity.Admin;
import com.fikiap.chatbrobackendapi.service.AdminService;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



/**
 * Controller untuk mengelola operasi terkait Admin.
 */
@RestController
@RequestMapping("/api/admins")
public class AdminController {
    private final AdminService adminService;

    /**
     * Konstruktor AdminController.
     *
     * @param adminService Layanan AdminService yang digunakan.
     */
    @Autowired
    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    /**
     * Menyimpan admin baru.
     *
     * @param admin Objek Admin yang akan disimpan.
     * @return ResponseEntity dengan status HTTP dan pesan yang sesuai.
     */
    @PostMapping
    public ResponseEntity<?> saveAdmin(@Valid @RequestBody Admin admin) {
        try {
            return adminService.saveAdmin(admin);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Gagal menyimpan admin: " + e.getMessage());
        }
    }

    /**
     * Mengambil detail admin berdasarkan UID.
     *
     * @param uid UID admin yang akan diambil detailnya.
     * @return ResponseEntity dengan status HTTP dan data admin yang sesuai.
     */
    @GetMapping("/{uid}")
    public ResponseEntity<?> getAdmin(@PathVariable String uid) {
        try {
            return adminService.getAdminDetails(uid);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Gagal mengambil detail admin: " + e.getMessage());
        }
    }

    /**
     * Mengupdate data admin berdasarkan UID.
     *
     * @param uid   UID admin yang akan diupdate.
     * @param admin Objek Admin dengan data yang akan diupdate.
     * @return ResponseEntity dengan status HTTP dan pesan yang sesuai.
     */
    @PutMapping("/{uid}")
    public ResponseEntity<?> updateAdmin(@PathVariable String uid, @Valid @RequestBody Admin admin) {
        try {
            admin.setUid(uid);
            return adminService.updateAdmin(admin);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Gagal memperbarui admin: " + e.getMessage());
        }
    }

    /**
     * Menghapus admin berdasarkan UID.
     *
     * @param uid UID admin yang akan dihapus.
     * @return ResponseEntity dengan status HTTP dan pesan yang sesuai.
     */
    @DeleteMapping("/{uid}")
    public ResponseEntity<?> deleteAdmin(@PathVariable String uid) {
        try {
            return adminService.deleteAdmin(uid);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Gagal menghapus admin: " + e.getMessage());
        }
    }

    /**
     * Mengambil semua data admin.
     *
     * @return ResponseEntity dengan status HTTP dan data admin yang sesuai.
     */
    @GetMapping
    public ResponseEntity<?> getAllAdmins() {
        try {
            return adminService.getAllAdmins();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Gagal mengambil data admin: " + e.getMessage());
        }
    }
}
