package com.fikiap.chatbrobackendapi.controller;

import com.fikiap.chatbrobackendapi.entity.Status;
import com.fikiap.chatbrobackendapi.service.StatusService;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



/**
 * Controller untuk mengelola operasi terkait Status.
 */
@RestController
@RequestMapping("/api/statuss")
public class StatusController {
    private final StatusService statusService;

    /**
     * Konstruktor StatusController.
     *
     * @param statusService Layanan StatusService yang digunakan.
     */
    @Autowired
    public StatusController(StatusService statusService) {
        this.statusService = statusService;
    }

    /**
     * Menyimpan status baru.
     *
     * @param status Objek Status yang akan disimpan.
     * @return ResponseEntity dengan status HTTP dan pesan yang sesuai.
     */
    @PostMapping
    public ResponseEntity<?> saveStatus(@Valid @RequestBody Status status) {
        try {
            return statusService.saveStatus(status);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Gagal menyimpan status: " + e.getMessage());
        }
    }

    /**
     * Mengambil detail status berdasarkan UID.
     *
     * @param uid UID status yang akan diambil detailnya.
     * @return ResponseEntity dengan status HTTP dan data status yang sesuai.
     */
    @GetMapping("/{uid}")
    public ResponseEntity<?> getStatus(@PathVariable String uid) {
        try {
            return statusService.getStatusDetails(uid);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Gagal mengambil detail status: " + e.getMessage());
        }
    }

    /**
     * Mengupdate data status berdasarkan UID.
     *
     * @param uid    UID status yang akan diupdate.
     * @param status Objek Status dengan data yang akan diupdate.
     * @return ResponseEntity dengan status HTTP dan pesan yang sesuai.
     */
    @PutMapping("/{uid}")
    public ResponseEntity<?> updateStatus(@PathVariable String uid, @Valid @RequestBody Status status) {
        try {
            status.setUid(uid);
            return statusService.updateStatus(status);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Gagal memperbarui status: " + e.getMessage());
        }
    }

    /**
     * Menghapus status berdasarkan UID.
     *
     * @param uid UID status yang akan dihapus.
     * @return ResponseEntity dengan status HTTP dan pesan yang sesuai.
     */
    @DeleteMapping("/{uid}")
    public ResponseEntity<?> deleteStatus(@PathVariable String uid) {
        try {
            return statusService.deleteStatus(uid);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Gagal menghapus status: " + e.getMessage());
        }
    }

    /**
     * Mengambil semua data status.
     *
     * @return ResponseEntity dengan status HTTP dan data status yang sesuai.
     */
    @GetMapping
    public ResponseEntity<?> getAllStatuss() {
        try {
            return statusService.getAllStatuss();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Gagal mengambil data status: " + e.getMessage());
        }
    }
}
