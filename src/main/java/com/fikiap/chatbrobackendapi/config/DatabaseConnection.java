package com.fikiap.chatbrobackendapi.config;

import com.google.cloud.firestore.Firestore;
import com.google.firebase.cloud.FirestoreClient;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.DependsOn;

/**
 * Kelas Singleton untuk mengelola koneksi ke database.
 */
@Component
@DependsOn("firebaseInitialization")
public class DatabaseConnection {
    private static DatabaseConnection instance;
    private Firestore dbFirestore;

    private DatabaseConnection() {
        // Inisialisasi koneksi ke database di sini
        dbFirestore = FirestoreClient.getFirestore();
    }

    /**
     * Mengembalikan instans dari DatabaseConnection.
     *
     * @return Instans dari DatabaseConnection
     */
    public static synchronized DatabaseConnection getInstance() {
        if (instance == null) {
            instance = new DatabaseConnection();
        }
        return instance;
    }

    /**
     * Mengembalikan instans Firestore untuk mengakses database.
     *
     * @return Instans Firestore
     */
    public Firestore getFirestore() {
        return dbFirestore;
    }
}
